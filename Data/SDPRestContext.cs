﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SDP.Rest.Models;

namespace SDP.Rest.Data
{
    public class SDPRestContext : DbContext
    {
        public SDPRestContext (DbContextOptions<SDPRestContext> options)
            : base(options)
        {
        }

        public DbSet<SDP.Rest.Models.Student> Student { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasData(
                new Student { Id = 1, FirstName = "Adam", LastName = "Kowalski" },
                new Student { Id = 2, FirstName = "Alicja", LastName = "Nowak" },
                new Student { Id = 3, FirstName = "Martyna", LastName = "Stepien" },
                new Student { Id = 4, FirstName = "Jacek", LastName = "Placek" },
                new Student { Id = 5, FirstName = "Piotr", LastName = "Wolski" },
                new Student { Id = 6, FirstName = "John", LastName = "Snow" },
                new Student { Id = 7, FirstName = "Sam", LastName = "Witwicky" },
                new Student { Id = 8, FirstName = "Tony", LastName = "Stark" },
                new Student { Id = 9, FirstName = "Eddard", LastName = "Stark" },
                new Student { Id = 10, FirstName = "Arya", LastName = "Stark" }
            );
        }
    }
}

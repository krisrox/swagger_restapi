﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using SDP.Rest.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SDP.Rest.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private List<Student> students = new StudentsCollection();

        // GET: api/<StudentController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(students);
        }

        // GET api/<StudentController>/5
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult Get(int id)
        {
            Student student = students.FirstOrDefault(s => s.Id == id);

            if (student == null)
                return NotFound();
            else
                return Ok(student);
        }

        [HttpGet]
        [Route("{name}")]
        public IActionResult Get(string name)
        {
            var student = students.FirstOrDefault(s => s.FirstName.Contains(name));

            if (student == null)
                return NotFound();
            else
                return Ok(student);
        }

        // POST api/<StudentController>
        [HttpPost]
        public IActionResult Post([FromBody] Student newStudent)
        {
            newStudent.Id = students.Count();
            students.Add(newStudent);

            return Ok(students);
            //return CreatedAtAction(nameof(Get), new { id = newStudent.Id }, newStudent);
        }

        // PUT api/<StudentController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Student updateStudent)
        {
            var student = students.FirstOrDefault(s => s.Id == id);

            if (student != null)
            {
                student.FirstName = updateStudent.FirstName;
                student.LastName = updateStudent.LastName;

                return Ok(students);
            }

            return NotFound();
        }

        // DELETE api/<StudentController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var student = students.FirstOrDefault(s => s.Id == id);
            if (student != null)
            {
                students.Remove(student);

                return Ok(students);
            }
            else
            {
                return NotFound();
            }
        }
    }
}

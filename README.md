# SDP #7

## Table of contents
* [General info](#general-info)
* [Authors](#authors)

## General info

.Net Core web app to handle REST API functionality. Extended with Entity Framework and JWT authentication.

## Author

*Michał Staruch*





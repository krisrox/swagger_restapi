﻿using System.Collections.Generic;

namespace SDP.Rest.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }

    public class StudentsCollection : List<Student>
    {
        public StudentsCollection()
        {
            var students = new Student[]
            {
                new Student { Id = 0, FirstName = "Adam", LastName = "Kowalski" },
                new Student { Id = 1, FirstName = "Alicja", LastName = "Nowak" },
                new Student { Id = 2, FirstName = "Martyna", LastName = "Stepien" },
                new Student { Id = 3, FirstName = "Jacek", LastName = "Placek" },
            };
            this.AddRange(students);
        }
    }
}
